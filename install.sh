#!/bin/bash
# ====================================================
#   Copyright (C)2019 All rights reserved.
#
#   Author        : licg
#   Email         : sparkwalk@gmail.com
#   File Name     : install.sh
#   Last Modified : 2019-03-05 14:51
#   Describe      : fast install vim plugs.
#
# ====================================================

CURRENT_DIR=`pwd`
FOR_VIM=true

echo "Step1: setting up symlinks"
if $FOR_VIM; then
    ln -f -s $CURRENT_DIR/vimrc $HOME/.vimrc
    ln -f -s $CURRENT_DIR/vimrc.bundles $HOME/.vimrc.bundles
    rm -f $HOME/.vim
    ln -f -s "$CURRENT_DIR/vim" "$HOME/.vim"
    ln -f -s $CURRENT_DIR/vimrc.ctags $HOME/.vimrc.ctags
    ln -f -s $CURRENT_DIR/tmux.conf $HOME/.tmux.conf
fi

echo "Step2: update/install plugins using Vim-plug"
system_shell=$SHELL
export SHELL="/bin/sh"
if $FOR_VIM; then
    vim -u $HOME/.vimrc.bundles +PlugInstall! +PlugClean! +qall
fi
export SHELL=$system_shell

echo "Install Done!"
